configfile: "config.yaml"
conda: "environment.yaml"

IDS, = glob_wildcards("input/{id}.cram")

rule all:
#Ultimate targets are md5s of completed fastqs
    input:  expand("output/{id}{direction}.fastq.gz.md5",id=IDS,direction=['_R1','_R2'])

rule check_cram_md5s:
#Check md5s of incoming bam files
	input:
		cram_md5 = "input/{id}.cram.md5"
	output:
		md5_check_cram = temp("temp/{id}.cram.md5.OK")
	log:
		"logs/{id}.md5.log"
	resources:
		runtime = 180
	shell:
		"""
		cd input; if md5sum --status -c {wildcards.id}.cram.md5 2> ../{log};
		then echo 'bam file md5 checksum: OK' > ../{output.md5_check_cram};
		else echo 'bam file md5 checksum: failed' > ../{log};
		fi
		"""

rule extract_reads:
#Extract all combinations of unmapped reads - mark as temp
	input:
		md5_check_cram = "temp/{id}.cram.md5.OK",
		cram = "input/{id}.cram"
	output:
		read_unmapped = "work/{id}.read.unmapped.bam",
		mate_unmapped = "work/{id}.mate.unmapped.bam",
		pair_unmapped = "work/{id}.pair.unmapped.bam",
		read_unmapped_quickcheck = temp("temp/{id}.read.unmapped.quickcheck.OK"),
		mate_unmapped_quickcheck = temp("temp/{id}.mate.unmapped.quickcheck.OK"),
		pair_unmapped_quickcheck = temp("temp/{id}.pair.unmapped.quickcheck.OK"),
		read_unmapped_flagstat = "flagstat/{id}.read.unmapped.flagstat",
                mate_unmapped_flagstat = "flagstat/{id}.mate.unmapped.flagstat",
                pair_unmapped_flagstat = "flagstat/{id}.pair.unmapped.flagstat",
	log:
		"logs/{id}.log.txt"
	params:
		ref=config["reference_genome"]
	resources:
		mem_gb = 2,
		runtime = 1440
	conda: "environment.yaml"
	shell:
		"python scripts/extract_reads.py {params.ref} {input.cram} {output.read_unmapped} {output.mate_unmapped} {output.pair_unmapped} {output.read_unmapped_quickcheck} {output.mate_unmapped_quickcheck} {output.pair_unmapped_quickcheck} {output.read_unmapped_flagstat} {output.mate_unmapped_flagstat} {output.pair_unmapped_flagstat} {log}"

rule sort_bams:
#Bams should be sorted before merging
        input:
                read_unmapped = "work/{id}.read.unmapped.bam",
		mate_unmapped = "work/{id}.mate.unmapped.bam",
		pair_unmapped = "work/{id}.pair.unmapped.bam",
		read_unmapped_quickcheck = "temp/{id}.read.unmapped.quickcheck.OK",
                mate_unmapped_quickcheck = "temp/{id}.mate.unmapped.quickcheck.OK",
                pair_unmapped_quickcheck = "temp/{id}.pair.unmapped.quickcheck.OK"
	output:
		read_unmapped_sorted = temp("work/{id}.read.unmapped.sorted.bam"),
		mate_unmapped_sorted = temp("work/{id}.mate.unmapped.sorted.bam"),
		pair_unmapped_sorted = temp("work/{id}.pair.unmapped.sorted.bam"),
		read_unmapped_sorted_quickcheck = "temp/{id}.read.unmapped.sorted.quickcheck.OK",
                mate_unmapped_sorted_quickcheck = "temp/{id}.mate.unmapped.sorted.quickcheck.OK",
                pair_unmapped_sorted_quickcheck = "temp/{id}.pair.unmapped.sorted.quickcheck.OK",
		read_unmapped_sorted_flagstat = "flagstat/{id}.read.unmapped.sorted.flagstat",
                mate_unmapped_sorted_flagstat = "flagstat/{id}.mate.unmapped.sorted.flagstat",
                pair_unmapped_sorted_flagstat = "flagstat/{id}.pair.unmapped.sorted.flagstat"
	log:
		"logs/{id}.log.txt"
	resources:
		mem_gb = 3,
		runtime = 1440
	conda: "environment.yaml"
	shell:
		"python scripts/sort_bams.py {wildcards.id} {input.read_unmapped} {input.mate_unmapped} {input.pair_unmapped} {output.read_unmapped_sorted} {output.mate_unmapped_sorted} {output.pair_unmapped_sorted} {output.read_unmapped_sorted_quickcheck} {output.mate_unmapped_sorted_quickcheck} {output.pair_unmapped_sorted_quickcheck} {output.read_unmapped_sorted_flagstat} {output.mate_unmapped_sorted_flagstat} {output.pair_unmapped_sorted_flagstat} {log}"

rule merge_bams:
#Combine unmapped bam subsets to a single bam
	input:
		read_unmapped_sorted = "work/{id}.read.unmapped.sorted.bam",
		mate_unmapped_sorted = "work/{id}.mate.unmapped.sorted.bam",
		pair_unmapped_sorted = "work/{id}.pair.unmapped.sorted.bam",
		read_unmapped_sorted_quickcheck = "temp/{id}.read.unmapped.sorted.quickcheck.OK",
		mate_unmapped_sorted_quickcheck = "temp/{id}.mate.unmapped.sorted.quickcheck.OK",
		pair_unmapped_sorted_quickcheck = "temp/{id}.pair.unmapped.sorted.quickcheck.OK"
	output:
		merged_bam = temp("work/{id}.merged.bam"),
		merged_bam_quickcheck = temp("temp/{id}.merged.quickcheck.OK"),
		merged_bam_flagstat = "flagstat/{id}.merged.flagstat"
	log:
		"logs/{id}.log.txt"
	resources:
		mem_gb = 3,
		runtime = 1440
	conda: "environment.yaml"
	shell:
		"""
		samtools merge -n {output.merged_bam} {input.read_unmapped_sorted} {input.mate_unmapped_sorted} {input.pair_unmapped_sorted}
		if samtools quickcheck {output.merged_bam};
		then echo 'bam file quickcheck: OK' > {output.merged_bam_quickcheck};
		else echo 'bam file quickcheck: failed' >> {log};
		fi
		samtools flagstat {output.merged_bam} > {output.merged_bam_flagstat}
		"""

rule covertToFastq:
#Convert merged bam into forward and reverse fastqs
	input:
		merged_bam_quickcheck = "temp/{id}.merged.quickcheck.OK",
		merged_bam = "work/{id}.merged.bam"
	output:
		fastq_R1 = "output/{id}_R1.fastq.gz",
		fastq_R2 = "output/{id}_R2.fastq.gz"
	resources:
		runtime = 1440
	conda: "environment.yaml"
	shell:
		"""
		samtools fastq {input.merged_bam} -1 {output.fastq_R1} -2 {output.fastq_R2}
		"""

rule generate_fastq_md5:
#Produce md5sums for completed fastqs
	input:
		fastq_R1 = "output/{id}_R1.fastq.gz",
		fastq_R2 = "output/{id}_R2.fastq.gz"
	output:
		md5_R1 = "output/{id}_R1.fastq.gz.md5",
		md5_R2 = "output/{id}_R2.fastq.gz.md5"
	resources:
		runtime = 180
	run:
		shell("cd output; md5sum {wildcards.id}_R1.fastq.gz > {wildcards.id}_R1.fastq.gz.md5 ")
		shell("cd output; md5sum {wildcards.id}_R2.fastq.gz > {wildcards.id}_R2.fastq.gz.md5 ")
			
