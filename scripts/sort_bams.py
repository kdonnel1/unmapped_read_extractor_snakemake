#!/bin/python

import sys
import subprocess

sample_id = sys.argv[1] 
read_unmapped = sys.argv[2]
mate_unmapped = sys.argv[3]
pair_unmapped = sys.argv[4]
read_unmapped_sorted = sys.argv[5]
mate_unmapped_sorted = sys.argv[6]
pair_unmapped_sorted = sys.argv[7]
read_unmapped_sorted_quickcheck = sys.argv[8]
mate_unmapped_sorted_quickcheck = sys.argv[9]
pair_unmapped_sorted_quickcheck = sys.argv[10]
read_unmapped_sorted_flagstat = sys.argv[11]
mate_unmapped_sorted_flagstat = sys.argv[12]
pair_unmapped_sorted_flagstat = sys.argv[13]
log = sys.argv[14]

#Create dir for temp files
tmpdir = sample_id + '_tmp'

subprocess.call("mkdir " + tmpdir,shell=True) 

subprocess.call("samtools sort " + read_unmapped  + " -o " + read_unmapped_sorted + " -T " + tmpdir + " 2> " + log,shell=True)
subprocess.call("rmdir " + tmpdir,shell=True)

subprocess.call("samtools sort " + mate_unmapped  + " -o " + mate_unmapped_sorted + " -T " + tmpdir + " 2> " + log,shell=True)
subprocess.call("rmdir " + tmpdir,shell=True)

subprocess.call("samtools sort " + pair_unmapped  + " -o " + pair_unmapped_sorted + " -T " + tmpdir + " 2> " + log,shell=True)
subprocess.call("rmdir " + tmpdir,shell=True)

#Perform quickcheck to evaluate subsample bams
subprocess.call("if samtools quickcheck " + read_unmapped_sorted + " 2> " + log + " ; then echo 'read unmapped sorted quickcheck: OK' > " + read_unmapped_sorted_quickcheck + "; else echo 'read unmapped sorted quickcheck: failed' > " + log + " ; fi",shell=True)
subprocess.call("if samtools quickcheck " + mate_unmapped_sorted + " 2> " + log + " ; then echo 'mate unmapped sorted quickcheck: OK' > " + mate_unmapped_sorted_quickcheck + "; else echo 'mate unmapped sorted quickcheck: failed' > " + log + " ; fi",shell=True)
subprocess.call("if samtools quickcheck " + pair_unmapped_sorted + " 2> " + log + " ; then echo 'pair unmapped sorted quickcheck: OK' > " + pair_unmapped_sorted_quickcheck + "; else echo 'pair unmapped sorted quickcheck: failed' > " + log + " ; fi",shell=True)

#Finally perform flagstat on subsample bams
subprocess.call("samtools flagstat " + read_unmapped_sorted + " > " + read_unmapped_sorted_flagstat + " 2> " + log,shell=True)
subprocess.call("samtools flagstat " + mate_unmapped_sorted + " > " + mate_unmapped_sorted_flagstat + " 2> " + log,shell=True)
subprocess.call("samtools flagstat " + pair_unmapped_sorted + " > " + pair_unmapped_sorted_flagstat + " 2> " + log,shell=True)



