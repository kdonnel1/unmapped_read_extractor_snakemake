#!/bin/python

import sys
import subprocess

params_ref = sys.argv[1] 
input_cram = sys.argv[2]
output_read_unmapped = sys.argv[3]
output_mate_unmapped = sys.argv[4]
output_pair_unmapped = sys.argv[5]
output_read_unmapped_quickcheck = sys.argv[6]
output_mate_unmapped_quickcheck = sys.argv[7]
output_pair_unmapped_quickcheck = sys.argv[8]
output_read_unmapped_flagstat = sys.argv[9]
output_mate_unmapped_flagstat = sys.argv[10]
output_pair_unmapped_flagstat = sys.argv[11]
log = sys.argv[12]

#Extract unmapped reads whose mate is mapped
subprocess.call("samtools view -u -f 4 -F264 " + input_cram + " -T " + params_ref + " > " + output_read_unmapped + " 2> " + log,shell=True)

#Extract mapped reads whose mate is unmapped
subprocess.call("samtools view -u -f 8 -F 260 " + input_cram + " -T " + params_ref + " > " + output_mate_unmapped + " 2> " + log,shell=True)

#Extract reads where pair is unmapped
subprocess.call("samtools view -u -f 12 -F 256 " + input_cram + " -T " + params_ref + " > " + output_pair_unmapped + " 2> " + log,shell=True)

#Perform quickcheck to evaluate subsample bams
subprocess.call("if samtools quickcheck " + output_read_unmapped + " 2> " + log + " ; then echo 'read unampped quickcheck: OK' > " + output_read_unmapped_quickcheck + "; else echo 'read unmapped quickcheck: failed' > " + log + " ; fi",shell=True)
subprocess.call("if samtools quickcheck " + output_mate_unmapped + " 2> " + log + " ; then echo 'mate unampped quickcheck: OK' > " + output_mate_unmapped_quickcheck + "; else echo 'mate unmapped quickcheck: failed' > " + log + " ; fi",shell=True)
subprocess.call("if samtools quickcheck " + output_pair_unmapped + " 2> " + log + " ; then echo 'pair unampped quickcheck: OK' > " + output_pair_unmapped_quickcheck + "; else echo 'pair unmapped quickcheck: failed' > " + log + " ; fi",shell=True)

#Finally perform flagstat on subsample bams
subprocess.call("samtools flagstat " + output_read_unmapped + " > " + output_read_unmapped_flagstat + " 2> " + log,shell=True)
subprocess.call("samtools flagstat " + output_mate_unmapped + " > " + output_mate_unmapped_flagstat + " 2> " + log,shell=True)
subprocess.call("samtools flagstat " + output_pair_unmapped + " > " + output_pair_unmapped_flagstat + " 2> " + log,shell=True)




