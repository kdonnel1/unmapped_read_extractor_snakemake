A Snakemake pipeline for automated extraction of unmapped reads from cram files.

* To run, first initialise a conda environment with Snakemake available. 

* Place all cram file within a directory named 'input' with their accompanying md5sums. 

* Launch the pipeline using 'snakemake --profile cluster-qsub --cluster-config cluster_config.yaml --use-conda'

The pipeline extracts read pairs in which one or both reads are unmapped; these are then stored as fastq in the output directory.

Intermediate bamfiles are deleted with the exception of those initially generated that contain the unmapped reads. 
This is as the initial step is by far the most time consuming, and undesriable to repeat in the event of a restart - these should be manually removed.
